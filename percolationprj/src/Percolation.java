import edu.princeton.cs.algs4.WeightedQuickUnionUF;

public class Percolation {
    private static final byte BLOCKED = 0;
    private static final byte OPENED = 1;
    private static final byte TOP = 1 << 2;
    private static final byte BOTTOM = 1 << 3;
    
    private WeightedQuickUnionUF uf;
    private int gridSize;
    private byte[] sites;
    private boolean isPercolate;

    public Percolation(int N) {
        if (N <= 0) {
            throw new IllegalArgumentException();
        }
        
        gridSize = N;
        sites = new byte[N*N];
        for (int i = 1; i <= gridSize; ++i) {
            for (int j = 1; j <= gridSize; ++j) {
                sites[xyTo1D(i, j)] = BLOCKED;
            }
        }
        
        isPercolate = false;
        
        uf = new WeightedQuickUnionUF(N*N);
    }

    public void open(int i, int j) {
        if (!checkParams(i, j))
            throw new IndexOutOfBoundsException();
        
        int p = xyTo1D(i, j);
        int q;
        
        
        sites[p] |= OPENED;
        
        if (i == 1) {
            sites[p] |= TOP;
        }
        
        if (i == gridSize) {
            sites[p] |= BOTTOM;
        }
        
        byte status = sites[p];
        
        q = xyTo1D(i - 1, j);
        if (checkParams(i - 1, j) && (sites[q] & OPENED) != 0) {
            status |= sites[uf.find(q)];
            uf.union(p, q);
            
        }
        
        q = xyTo1D(i + 1, j);
        if (checkParams(i + 1, j) && (sites[q] & OPENED) != 0) {
            status |= sites[uf.find(q)];
            uf.union(p, q);
        }
        
        q = xyTo1D(i, j - 1);
        if (checkParams(i, j - 1) && (sites[q] & OPENED) != 0) {
            status |= sites[uf.find(q)];
            uf.union(p, q);
        }
        
        q = xyTo1D(i, j + 1);
        if (checkParams(i, j + 1) && (sites[q] & OPENED) != 0) {
            status |= sites[uf.find(q)];
            uf.union(p, q);
        }
        
        int newRoot = uf.find(p);
        sites[newRoot] |= status;
        if ((sites[newRoot] & TOP) != 0 && (sites[newRoot] & BOTTOM) != 0) {
            isPercolate = true;
        }
    }

    public boolean isOpen(int i, int j) {    // is site (row i, column j) open?
        if (!checkParams(i, j))
            throw new IndexOutOfBoundsException();
        
        return (sites[xyTo1D(i, j)] & OPENED) != 0;
    }

    public boolean isFull(int i, int j) {    // is site (row i, column j) full?
        if (!checkParams(i, j))
            throw new IndexOutOfBoundsException();
        
        int p = xyTo1D(i, j);
        return (sites[uf.find(p)] & TOP) != 0;
    }

    public boolean percolates() {            // does the system percolate?
        return isPercolate;
    }

    public static void main(String[] args) { // test client (optional)
        Percolation p = new Percolation(2);
        p.open(1, 1);
        p.open(2, 2);
        p.open(1, 2);
        
        
        System.out.println(p.uf.connected(p.xyTo1D(1, 1), p.xyTo1D(2, 1)));
        System.out.println(p.isFull(1, 1));
        System.out.println(p.percolates());
    }
    
    private int xyTo1D(int x, int y) {
        return (x - 1) * gridSize + y - 1;
    }
    
    private boolean checkParams(int x, int y) {
        return x >= 1 && x <= gridSize && y >= 1 && y <= gridSize;
    }
}

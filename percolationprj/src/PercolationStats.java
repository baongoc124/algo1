
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;

public class PercolationStats {

    private int n, t;
    private double[] thresholds;

    public PercolationStats(int N, int T) {
        if (N <= 0 || T <= 0) {
            throw new IllegalArgumentException();
        }
        n = N;
        t = T;

        thresholds = new double[t];
        for (int i = 0; i < t; ++i) {
            Percolation p = new Percolation(n);

            int j, u, v;
            int count = 0;
            int[] indexes = new int[n * n];
            for (j = 0; j < n * n; ++j) {
                indexes[j] = j;
            }
            StdRandom.shuffle(indexes);
            
            while (!p.percolates()) {
                j = indexes[count];
                u = j / n + 1;
                v = j % n + 1;

                p.open(u, v);
                ++count;
            }

            thresholds[i] = count / (double) (n * n);

        }
    }

    public double mean() {
        return StdStats.mean(thresholds);
    }

    public double stddev() {
        return StdStats.stddevp(thresholds);
    }

    public double confidenceLo() {
        return mean() - 1.96 * stddev() / Math.sqrt(t);
    }

    public double confidenceHi() {
        return mean() + 1.96 * stddev() / Math.sqrt(t);
    }

    public static void main(String[] args) {
        int n = Integer.parseInt(args[0]);
        int t = Integer.parseInt(args[1]);

        PercolationStats ps = new PercolationStats(n, t);
        StdOut.printf("mean                    = %f%n", ps.mean());
        StdOut.printf("stddev                  = %f%n", ps.stddev());
        StdOut.printf("95%% confidence interval = %f, %f%n", ps.confidenceLo(),
                ps.confidenceHi());
    }
}
